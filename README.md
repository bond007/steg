# steg

Steganography software written in Python

## Introduction

steg.py hides messages in the least significant bits of a cover image.
It also recovers messages which are hidden in an image.

```
usage: steg.py [-h] [-u] [-n] [-c COVER_FILE] [-m MSG_FILE] -s STEG_FILE

Steganography hider and unhider.

options:
  -h, --help     show this help message and exit
  -u             unhide message (default: hide)
  -n             use default password (default: request password)
  -c COVER_FILE  cover image file name (mandatory when hiding)
  -m MSG_FILE    message file name (default: stdin/stdout)
  -s STEG_FILE   image with hidden message file name (mandatory)
```

Messages can be any arbitrary file. If a message filename is not
specified, then the message is read from stdin when hiding inside a
cover image, and is written to stdout when unhiding from an image. The
cover image must be in a format readable by the Python Imaging Library
(PIL). The output image file, containing the hidden message, is always
in WebP format.

##  Method

Hiding a message in a cover image is a multi-step process:

1. Read the cover image into a flattened array of RGB bytes.
2. Read the message into a byte array.
3. Compress the message using LZMA.
4. Encrypt the message.
4. Unpack the compressed message bytes into a bit array.
5. Replace the least significant bits of pseudo-randomly located bytes
   in the cover image with the bits from the message.
6. Save the modified cover image to a PNG file.

Unhiding messages is essentially the above process in reverse.

The reason for compressing the messages before they are hidden is that
we want to modify as few pixels in the cover image as possible. The more
pixels that are modified, the more likely it is that analysis will find
there is message hidden in the image.

Encryption uses the XSalsa20 stream cipher with Poly1305 MAC
authentication.  These are supplied by the well known pyNaCl library.

The reason for modifying random bytes in the cover image, instead of
starting at the first byte, is to make it a little harder to identify
that a message has been hidden in the image. If we modify bytes starting
at the first byte, then we find a block of bytes at the beginning of the
image whose least significant bits have quite different statistical
properties to the bytes in the remainder of the image.  By dispersing
the modified bytes at random, the image modification is harder to
detect. We use a cryptographic pseudo-random number generator with a
seed determined from the password to generate the list of bytes which
are modified. This is trivial for the decoder to generate, provided they
use the same seed.

## Examples

To hide the message in pg61.txt in the cover image called original.png
and save the result is steg.png:

```
steg.py -c original.png -m pg61.txt -s steg.png
```

To hide a message from stdin in original.png, and use a default password
(instead of prompting the user for a password):

```
steg.py -c orginal.png -n -s steg.png
```

To recover the message from steg.png:

```
steg.py -s steg.png -u
```

## Examples: Part 2

![red channel](example/diff_R.png)
![green channel](example/diff_G.png)
![blue channe](example/diff_B.png)
