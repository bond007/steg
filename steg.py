#!/usr/bin/env python3

"""
Steganographic encoder and decoder.

Hide encrypted messages in the least significant bits of an image.

USER CONFIGURABLE SETTINGS.

The user may want to alter the settings below.

CRYPTOGRAPHIC SETTINGS

DEFAULT_PASSWORD: The password if the -n command line option is
specified.  Do not expect changing this will alter the security; the
default password is sitting in clear text in the source code.

KDF: The key derivation function used to convert the password to a
cryptographic key. This is currently set to Argon 2id. This is
considered a good function for general purpose use.

ARGON_OPS_LIMIT and ARGON_MEMLIMIT: These specify how much work the key
derivation function needs to do to generate a key. The more work, the
more secure is the key derivation. But this comes at the expense of
increased memory usage and computing time. The value set by default is
moderate; on my desktop the higher sensitive setting takes a very long
time. The interactive level is much faster, but at the expense of
security.

DETERMINISTIC_RNG_SEED_SIZE: This is how long the seed for the
generation of deterministic pseudo-random numbers is. This probably
won't need changing unless a different seed length is used in later
versions of libsodium.

STEGANOGRAPHIC IMAGE SETTINGS

IMG_ARGS: These settings are used by PIL to save the steganographic
image. They should be settings for a lossless image format. The default
is WepP, which provides good lossless compression. Alternatives might be
PNG (more widely supported) and so on. Refer to the PIL documentation
for the various settings which you will need to specify.
"""
import argparse
import getpass
import lzma
import sys
from typing import Tuple
from nacl import pwhash, secret, utils, exceptions
from PIL import Image
import numpy as np

DEFAULT_PASSWORD = b"James Bond 007"
KDF = pwhash.argon2id.kdf
ARGON_OPSLIMIT = pwhash.argon2id.OPSLIMIT_MODERATE
ARGON_MEMLIMIT = pwhash.argon2id.MEMLIMIT_MODERATE
DETERMINISTIC_RNG_SEED_SIZE = 32
IMG_ARGS = {"format": "WebP", "lossless": True, "quality": 100}


def hide(cover_file: str, steg_file: str, msg_file: str, password: bytes) -> None:
    """Hide a message file inside the cover image."""
    # Read the cover image.
    img_shape, img_data = read_img(file=cover_file)

    # Read the message from the specified file, or otherwise stdin.
    msg_bytes = read_msg(msg_file)

    # Compress the message.
    msg_bytes = lzma.compress(msg_bytes)

    # Encrypt the message.
    msg_bytes, salt = encrypt_message(message=msg_bytes, password=password)

    # Convert message to numpy array of bits.
    msg_bits = msg_to_bits(salt, msg_bytes)
    if msg_bits.size > img_data.size:
        raise Exception("Error: message too large for cover image.")

    msg_pct = 100 * msg_bits.size / img_data.size
    print(f"Percentage of cover image LSBs modified: {msg_pct:-5.2f}")

    # Put the message's bits in the least significant bits of pseudo-random
    # pixels in the image.
    bit_locs = rnd_locs(img_data.size, password)[: msg_bits.size]
    img_data[bit_locs] = np.bitwise_and(img_data[bit_locs], 254)
    img_data[bit_locs] = np.bitwise_or(img_data[bit_locs], msg_bits)

    # Save the image containing the message.
    save_img(file=steg_file, img_data=img_data, img_shape=img_shape)


def unhide(steg_file: str, msg_file: str, password: bytes) -> None:
    """Recover hidden message from stge_file."""
    _, steg_data = read_img(file=steg_file)

    # Extract the various message parts from the LSBs.
    img_bits = np.bitwise_and(steg_data, 1)
    salt, msg_bytes = extract_msg(img_bits=img_bits, password=password)

    # Decrypt the message.
    msg_bytes = decrypt_msg(salt=salt, msg_bytes=msg_bytes, password=password)

    # Uncompress the message.
    try:
        unc_bytes = lzma.decompress(msg_bytes)
    except lzma.LZMAError:
        # Fool a potential attacker.
        unc_bytes = msg_bytes

    # Output the message to a file or stdout.
    save_msg(msg_file=msg_file, unc_bytes=unc_bytes)


def save_msg(msg_file: str, unc_bytes: bytes) -> None:
    """Save the extracted message to a file, or print to stdout."""
    if msg_file is not None:
        try:
            with open(msg_file, "wb") as binaryfile:
                binaryfile.write(unc_bytes)
        except Exception:
            print("Error: unable to write message file.", file=sys.stderr)
            raise
    else:
        try:
            sys.stdout.buffer.write(unc_bytes)
        except Exception:
            print("Error: unable to write message to stdout.", file=sys.stderr)
            raise


def decrypt_msg(salt: bytes, msg_bytes: bytes, password: bytes) -> bytes:
    """Decrypt an encrypted message."""
    key = KDF(
        secret.SecretBox.KEY_SIZE,
        password,
        salt,
        opslimit=ARGON_OPSLIMIT,
        memlimit=ARGON_MEMLIMIT,
    )
    box = secret.SecretBox(key)
    try:
        msg_bytes = box.decrypt(msg_bytes)
    except exceptions.CryptoError:
        # Fool a potential attacker with random bytes.
        msg_bytes = utils.random(int(len(msg_bytes)))
    return msg_bytes


def read_msg(msg_file: str) -> bytes:
    """Read the message from the specified file, or otherwise stdin."""
    if msg_file is not None:
        try:
            with open(msg_file, "rb") as binaryfile:
                msg_bytes = bytearray(binaryfile.read())
        except Exception:
            print("Error: unable to read message file.", file=sys.stderr)
            raise
    else:
        try:
            msg_bytes = bytearray(sys.stdin.buffer.read())
        except Exception:
            print("Error: unable to read message from stdin.", file=sys.stderr)
            raise
    return msg_bytes


def read_img(file: str) -> Tuple[Tuple[int, ...], np.ndarray]:
    """
    Read and image and return it as flattened array of bytes and the
    original shape of the image.
    """
    try:
        img_data = np.asarray(Image.open(file), dtype=np.uint8)
    except Exception:
        print("Error: unable to read image file.", file=sys.stderr)
        raise
    return img_data.shape, img_data.flatten()


def msg_to_bits(salt: bytes, msg: bytes) -> np.ndarray:
    """Concatenate the salt and message, and convert required data to an array of bits."""
    msg_arr = np.frombuffer(salt, dtype=np.uint8)
    msg_arr = np.append(
        msg_arr, np.frombuffer(len(msg).to_bytes(4, byteorder="big"), dtype=np.uint8)
    )
    msg_arr = np.append(msg_arr, np.frombuffer(msg, dtype=np.uint8))
    return np.unpackbits(msg_arr)


def extract_msg(img_bits: np.ndarray, password: bytes) -> Tuple[bytes, bytes]:
    """
    Extract the salt and encrypted message from the least significant
    bits of the steganographic image.
    """
    # Unscramble the bits.
    bit_locs = rnd_locs(img_bits.size, password)
    img_bits = img_bits[bit_locs]

    # Get the salt.
    salt_len = pwhash.argon2id.SALTBYTES * 8
    salt = np.packbits(img_bits[:salt_len]).tobytes()

    # Get the number of bits in the message.
    size_bytes = np.packbits(img_bits[salt_len : 32 + salt_len]).tobytes()
    n_bits = int.from_bytes(size_bytes, byteorder="big") * 8
    if n_bits > (img_bits.size - salt_len - 32):
        # Fool a potential attacker.
        n_bits = img_bits.size - salt_len - 32

    # Get the message bytes.
    msg_bytes = np.packbits(img_bits[32 + salt_len : n_bits + 32 + salt_len]).tobytes()

    return salt, msg_bytes


def rnd_locs(img_size: int, password: bytes) -> np.ndarray:
    """Generate random locations in the image to hide the message."""
    seed = KDF(
        DETERMINISTIC_RNG_SEED_SIZE,
        password,
        b"\0" * pwhash.argon2id.SALTBYTES,
        opslimit=ARGON_OPSLIMIT,
        memlimit=ARGON_MEMLIMIT,
    )
    locs = np.arange(img_size, dtype=np.uint32)

    # Create some crytpographically secure random numbers for later shuffling.
    rnd_num = np.frombuffer(
        utils.randombytes_deterministic((img_size) * 4, seed), dtype=np.uint32
    )
    rnd_int = np.fix(rnd_num / (2**32 - 1) * np.arange(img_size, 0, -1)).astype(
        np.uint32
    ) + np.arange(img_size)

    # Shuffle the locations using the Fisher-Yates algorithm.
    for i in range(img_size):
        j = rnd_int[i]
        tmp = locs[i]
        locs[i] = locs[j]
        locs[j] = tmp

    return locs


def encrypt_message(message: bytes, password: bytes) -> Tuple[bytes, bytes]:
    """Encrypt the message."""
    salt = utils.random(pwhash.argon2i.SALTBYTES)
    key = KDF(
        secret.SecretBox.KEY_SIZE,
        password,
        salt,
        opslimit=ARGON_OPSLIMIT,
        memlimit=ARGON_MEMLIMIT,
    )
    box = secret.SecretBox(key)
    return box.encrypt(message), salt


def save_img(file: str, img_data: np.ndarray, img_shape: Tuple[int, ...]) -> None:
    """Save the image containing the message."""
    try:
        Image.fromarray(np.reshape(img_data, img_shape)).save(file, **IMG_ARGS)
    except Exception:
        print("Error: unable to write steganopgraphic image file.", file=sys.stderr)
        raise


def main() -> None:
    """Main subroutine."""
    parser = argparse.ArgumentParser(
        description="Steganography encoder and decode.", epilog="Author: Tim Hume"
    )
    parser.add_argument(
        "-u",
        action="store_true",
        dest="unh",
        help="unhide message (default: hide)",
    )
    parser.add_argument(
        "-n",
        action="store_true",
        dest="np",
        help="use default password (default: request password)",
    )
    parser.add_argument(
        "-c",
        action="store",
        dest="cover_file",
        default=None,
        type=str,
        help="cover image file name (mandatory when hiding)",
    )
    parser.add_argument(
        "-m",
        dest="msg_file",
        action="store",
        default=None,
        type=str,
        help="message file name (default: stdin/stdout)",
    )
    parser.add_argument(
        "-s",
        dest="steg_file",
        action="store",
        type=str,
        help="image with hidden message file name (mandatory)",
        required=True,
    )
    args = parser.parse_args()

    if not args.unh and args.cover_file is None:
        raise Exception("Cover image file required when hiding.")

    # Get the password, unless the default password is requested.
    if args.np:
        password = DEFAULT_PASSWORD
    else:
        password = bytes(getpass.getpass(), "utf-8")

    # Encode or decode the image as appropriate.
    if args.unh:
        unhide(steg_file=args.steg_file, msg_file=args.msg_file, password=password)
    else:
        hide(
            cover_file=args.cover_file,
            steg_file=args.steg_file,
            msg_file=args.msg_file,
            password=password,
        )


if __name__ == "__main__":
    main()
